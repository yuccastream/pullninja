#!/usr/bin/env bash

set -e

DEBUG=${DEBUG:-false}
if ${DEBUG}; then
    set -x
fi
SCRIPT_DIR="$(dirname $(readlink -f $0))"
cd ${SCRIPT_DIR}

CI=${CI:-false}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/yuccastream/pullninja'}
CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)}

export NOMAD_ADDR='https://nomad-infra.yuccastream.com'
export NOMAD_TOKEN=${NOMAD_TOKEN_infra:-''}

# Эти переменные не должны переопределяться через envsubst
export NOMAD_ALLOC_DIR='${NOMAD_ALLOC_DIR}'
export NOMAD_TASK_DIR='${NOMAD_TASK_DIR}'
export NOMAD_ALLOC_ID='${NOMAD_ALLOC_ID}'
export DOLLAR='$'


main() {
    if ! ${CI} ; then
        ${SCRIPT_DIR}/../build.sh docker
    fi
    export PATCH_IMAGE="${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
    envsubst < pullninja.nomad.tpl > pullninja.nomad
    nomad job validate pullninja.nomad
    if ! ${DEBUG}; then
        nomad job run pullninja.nomad
        rm -f pullninja.nomad
    fi
}

main $@
