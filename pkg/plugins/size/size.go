package size

import (
	"bufio"
	"regexp"
	"strings"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/labels"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

type size int

const (
	pluginName = "size"

	sizeXS size = iota
	sizeS
	sizeM
	sizeL
	sizeXL
	sizeXXL
)

var (
	labelPrefixRegExp = regexp.MustCompile(`^size\/\w+$`)
)

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	projectID := mr.Project.ID
	mrID := mr.ObjectAttributes.IID
	service := pc.GitLabClient.MergeRequests
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	m, _, err := service.GetMergeRequestChanges(projectID, mrID, nil)
	if err != nil {
		return err
	}
	size, err := getSizeLabel(m)
	if err != nil {
		return err
	}
	var labels []string
	for _, label := range m.Labels {
		if labelPrefixRegExp.MatchString(label) && label != size {
			continue
		}
		labels = append(labels, label)
	}
	labels = append(labels, size)
	labels = utils.RemoveDuplicatesUnordered(labels)
	if utils.IsSimilarLabels(labels, m.Labels) {
		return nil
	}
	gitlabLabels := gitlab.Labels(labels)
	options := &gitlab.UpdateMergeRequestOptions{
		Labels: &gitlabLabels,
	}
	_, _, err = service.UpdateMergeRequest(projectID, mrID, options, nil)
	return err
}

func calcChangesFromDiff(diff string) int {
	var lineCount int
	if len(diff) == 0 {
		return 0
	}
	scanner := bufio.NewScanner(strings.NewReader(diff))
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			continue
		}
		if strings.HasPrefix(line, "+") || strings.HasPrefix(line, "-") {
			lineCount++
		}
	}
	return lineCount
}

func getSizeLabel(m *gitlab.MergeRequest) (string, error) {
	var lineCount int
	for _, change := range m.Changes {
		diff := change.Diff
		lineCount += calcChangesFromDiff(diff)
	}
	size := bucket(lineCount)
	return size.label(), nil
}

func (s size) label() string {
	switch s {
	case sizeXS:
		return labels.SizeXS
	case sizeS:
		return labels.SizeS
	case sizeM:
		return labels.SizeM
	case sizeL:
		return labels.SizeL
	case sizeXL:
		return labels.SizeXL
	case sizeXXL:
		return labels.SizeXXL
	}

	return labels.SizeUnknown
}

func bucket(lineCount int) size {
	if lineCount < 10 {
		return sizeXS
	} else if lineCount < 30 {
		return sizeS
	} else if lineCount < 100 {
		return sizeM
	} else if lineCount < 500 {
		return sizeL
	} else if lineCount < 1000 {
		return sizeXL
	}

	return sizeXXL
}
