package milestone

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/labels"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName = "milestone"
)

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	var (
		labelV1 string
		labelV2 string
	)
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}
	if i.ObjectAttributes.MilestoneID <= 0 {
		return nil
	}
	iService := pc.GitLabClient.Issues
	issue, _, err := iService.GetIssue(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}
	if issue.Milestone != nil && issue.Milestone.DueDate != nil {
		labelV1 = labels.MilestonePrefix + issue.Milestone.DueDate.String()
	}
	if issue.Milestone != nil && len(issue.Milestone.Title) != 0 {
		labelV2 = labels.MilestonePrefix + issue.Milestone.Title
	}
	if isStringSliceContains(issue.Labels, labelV1) || isStringSliceContains(issue.Labels, labelV2) {
		return nil
	}
	if len(labelV2) == 0 {
		return nil
	}

	labelsNew := issue.Labels
	labelsNew = append(labelsNew, labelV2)
	labelsNew = utils.RemoveDuplicatesUnordered(labelsNew)
	if utils.IsSimilarLabels(labelsNew, issue.Labels) {
		return nil
	}

	_, _, err = pc.GitLabClient.Labels.CreateLabel(issue.ProjectID, createLabelOptions(labelV2, issue.Milestone), nil)
	if err != nil {
		logrus.Warnf("Failed to create label: %v", err)
	}

	issueUpdateOptions := &gitlab.UpdateIssueOptions{
		Labels: &labelsNew,
	}
	_, _, err = iService.UpdateIssue(issue.ProjectID, i.ObjectAttributes.IID, issueUpdateOptions, nil)
	return err
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	var (
		labelV1 string
		labelV2 string
	)
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	if mr.ObjectAttributes.MilestoneID <= 0 {
		return nil
	}
	mrService := pc.GitLabClient.MergeRequests
	m, _, err := mrService.GetMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}
	if m.Milestone != nil && m.Milestone.DueDate != nil {
		labelV1 = labels.MilestonePrefix + m.Milestone.DueDate.String()
	}
	if m.Milestone != nil && len(m.Milestone.Title) != 0 {
		labelV2 = labels.MilestonePrefix + m.Milestone.Title
	}
	if isStringSliceContains(m.Labels, labelV1) || isStringSliceContains(m.Labels, labelV2) {
		return nil
	}
	if len(labelV2) == 0 {
		return nil
	}

	labelsNew := m.Labels
	labelsNew = append(labelsNew, labelV2)
	labelsNew = utils.RemoveDuplicatesUnordered(labelsNew)
	if utils.IsSimilarLabels(labelsNew, m.Labels) {
		return nil
	}

	_, _, err = pc.GitLabClient.Labels.CreateLabel(mr.Project.ID, createLabelOptions(labelV2, m.Milestone), nil)
	if err != nil {
		logrus.Warnf("Failed to create label: %v", err)
	}

	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		Labels: &labelsNew,
	}
	_, _, err = mrService.UpdateMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, mergeUpdateOptions, nil)
	return err
}

func isStringSliceContains(s []string, e string) bool {
	for _, v := range s {
		if v == e {
			return true
		}
	}
	return false
}

func createLabelOptions(label string, m *gitlab.Milestone) *gitlab.CreateLabelOptions {
	return &gitlab.CreateLabelOptions{
		Name:        gitlab.String(label),
		Color:       gitlab.String(labels.MilestoneColor),
		Description: gitlab.String(fmt.Sprintf("Milestone %s since %s to %s", m.Title, m.StartDate.String(), m.DueDate.String())),
	}
}
