package lgtm

import (
	"regexp"
	"strings"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/labels"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName = "lgtm"
)

var (
	lgtmRe       = regexp.MustCompile(`(?mi)^/(lgtm|норм|ок|ok)\s*$`)
	lgtmCancelRe = regexp.MustCompile(`(?mi)^/(lgtm cancel|говно|гавно)\s*$`)
)

func init() {
	plugins.RegisterMergeCommentHandler(pluginName, handleMergeCommentEvent)
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
}

// handleMergeRequestEvent снимает аппрувы при обновлении Merge Request
func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	if mr.ObjectAttributes.State == "merged" || mr.ObjectAttributes.State == "closed" {
		return nil
	}
	mrService := pc.GitLabClient.MergeRequests
	m, _, err := mrService.GetMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}
	var labelsNew []string
	for _, label := range m.Labels {
		if strings.HasPrefix(label, labels.ReviewOKPrefix) {
			data := strings.Split(label, "/")
			if len(data) != 2 {
				continue
			}
			label = labels.ReviewNeededPrefix + data[1]
		}
		labelsNew = append(labelsNew, label)
	}
	gitlabLabels := gitlab.Labels(labelsNew)
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		Labels: &gitlabLabels,
	}
	_, _, err = mrService.UpdateMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, mergeUpdateOptions, nil)
	return err
}

func handleMergeCommentEvent(pc *plugins.PluginClient, mc *gitlab.MergeCommentEvent) error {
	author := mc.User.Username
	if pc.GitLabUser.Username == mc.User.Username {
		return nil
	}
	wantLGTM := false
	if lgtmRe.MatchString(mc.ObjectAttributes.Note) {
		wantLGTM = true
	} else if lgtmCancelRe.MatchString(mc.ObjectAttributes.Note) {
		wantLGTM = false
	} else {
		return nil
	}
	mrService := pc.GitLabClient.MergeRequests
	mr, _, err := mrService.GetMergeRequest(mc.ProjectID, mc.MergeRequest.IID, nil, nil)
	if err != nil {
		return err
	}
	labelsNew := mr.Labels
	if wantLGTM {
		labelsNew = removeFromLabels(labelsNew, labels.ReviewNeededPrefix+author)
		labelsNew = append(labelsNew, labels.ReviewOKPrefix+author)
	} else {
		labelsNew = removeFromLabels(labelsNew, labels.ReviewOKPrefix+author)
		labelsNew = append(labelsNew, labels.ReviewNeededPrefix+author)
	}
	labelsNew = utils.RemoveDuplicatesUnordered(labelsNew)
	if utils.IsSimilarLabels(labelsNew, mr.Labels) {
		return nil
	}
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		Labels: &labelsNew,
	}
	_, _, err = mrService.UpdateMergeRequest(mc.ProjectID, mc.MergeRequest.IID, mergeUpdateOptions, nil)
	return err
}

func removeFromLabels(labels []string, labelExcl string) []string {
	var result []string
	for _, label := range labels {
		if label == labelExcl {
			continue
		}
		result = append(result, label)
	}
	return result
}
