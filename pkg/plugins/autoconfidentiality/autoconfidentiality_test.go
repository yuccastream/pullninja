package autoconfidentiality

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

func Test_isConfidentialityUser(t *testing.T) {
	pc := &plugins.PluginClient{
		Config: &plugins.Config{},
	}
	assert.False(t, isConfidentialityUser(pc, "foobar"))
	pc.Config.AutoConfidentialityUsers = []string{
		"barfoo",
	}
	assert.False(t, isConfidentialityUser(pc, "foobar"))
	pc.Config.AutoConfidentialityUsers = []string{
		"barfoo",
		"foobar",
	}
	assert.True(t, isConfidentialityUser(pc, "foobar"))
}

func Test_isConfidentialityLabels(t *testing.T) {
	pc := &plugins.PluginClient{
		Config: &plugins.Config{},
	}
	assert.True(t, isConfidentialityLabels(pc, []gitlab.Label{}))
	assert.True(t, isConfidentialityLabels(pc, []gitlab.Label{
		{
			Name: "foobar",
		},
	}))
	pc.Config.AutoConfidentialityLabels = []string{
		"barfoo",
	}
	assert.False(t, isConfidentialityLabels(pc, []gitlab.Label{
		{
			Name: "foobar",
		},
	}))
	pc.Config.AutoConfidentialityLabels = []string{
		"barfoo",
		"foobar",
	}
	assert.True(t, isConfidentialityLabels(pc, []gitlab.Label{
		{
			Name: "foobar",
		},
	}))
}
