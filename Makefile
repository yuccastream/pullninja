.DEFAULT_GOAL := build

.PHONY: build
build:
	@echo "==> Building local..."
	@./build.sh bin

.PHONY: docker
docker:
	@echo "==> Building Docker image..."
	@./build.sh docker

.PHONY: test
test:
	@go test ./... -cover

.PHONY: test-report
test-report:
	@go test ./... -coverprofile=coverage.txt && go tool cover -html=coverage.txt
