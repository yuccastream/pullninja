package utils

import "testing"

func TestRemoveDuplicatesUnordered(t *testing.T) {
	tests := []struct {
		parts []string
		len   int
	}{
		{
			parts: []string{"a", "b", "c"},
			len:   3,
		},
		{
			parts: []string{"a", "b", "c", "c"},
			len:   3,
		},
		{
			parts: []string{"c", "c", "c", "c"},
			len:   1,
		},
	}
	for _, test := range tests {
		result := RemoveDuplicatesUnordered(test.parts)
		if len(result) != test.len {
			t.Errorf("Must be %d, but got %d", test.len, len(result))
		}
	}
}
