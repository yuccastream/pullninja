# Pull Ninja

Обработка webhook'ов от GitLab.

## Переменные окружения

* `GITLAB_ENDPOINT`
* `GITLAB_TOKEN`
* `GITLAB_WEBHOOK_SECRET_TOKEN`

## Плагины

Описание плагинов находится в их каталогах (`pkg/plugins/`).
