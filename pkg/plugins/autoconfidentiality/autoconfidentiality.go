package autoconfidentiality

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName = "autoconfidentiality"
)

func init() {
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func isConfidentialityUser(pc *plugins.PluginClient, username string) bool {
	for _, u := range pc.Config.AutoConfidentialityUsers {
		if u == username {
			return true
		}
	}
	return false
}

func isConfidentialityLabels(pc *plugins.PluginClient, labels []gitlab.Label) bool {
	if len(pc.Config.AutoConfidentialityLabels) == 0 {
		return true
	}
	for _, label := range pc.Config.AutoConfidentialityLabels {
		for _, issueLabel := range labels {
			if issueLabel.Name == label {
				return true
			}
		}
	}
	return false
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}
	if !isConfidentialityUser(pc, i.User.Username) {
		return nil
	}
	if !isConfidentialityLabels(pc, i.Labels) {
		return nil
	}
	opts := &gitlab.UpdateIssueOptions{
		Confidential: gitlab.Bool(true),
	}
	_, _, err := pc.GitLabClient.Issues.UpdateIssue(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID, opts, nil)
	return err
}
