package assignee

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName = "assignee"
)

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}
	if i.ObjectAttributes.Action != "open" {
		return nil
	}
	if i.ObjectAttributes.AssigneeID > 0 {
		return nil
	}
	issueUpdateOptions := &gitlab.UpdateIssueOptions{
		AssigneeIDs: []int{i.ObjectAttributes.AuthorID},
	}
	_, _, err := pc.GitLabClient.Issues.UpdateIssue(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID, issueUpdateOptions, nil)
	return err
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	if mr.ObjectAttributes.Action != "open" {
		return nil
	}
	if mr.ObjectAttributes.AssigneeID > 0 {
		return nil
	}
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		AssigneeID: gitlab.Int(mr.ObjectAttributes.AuthorID),
	}
	_, _, err := pc.GitLabClient.MergeRequests.UpdateMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, mergeUpdateOptions, nil)
	return err
}
