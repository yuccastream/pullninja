package labels

const (
	ReviewNeededPrefix = "review-needed/"
	ReviewOKPrefix     = "review-ok/"

	MilestonePrefix = "milestone/"
	MilestoneColor  = "#7F8C8D"

	SizeXS      = "size/XS"
	SizeS       = "size/S"
	SizeM       = "size/M"
	SizeL       = "size/L"
	SizeXL      = "size/XL"
	SizeXXL     = "size/XXL"
	SizeUnknown = "size/?"

	// KindEpic название лейбла, сигнализирующее о том, что
	// эта задача является эпиком, вынесено из kind, так как
	// задачи бывают большие, но оптимизационного характера
	// и хотелось бы их тоже делить по типам feature, bug и
	// что там еще добавиться в будущем
	KindEpic = "epic"
)
