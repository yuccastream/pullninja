package main

import (
	"flag"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"
)

const (
	defaultBaseURL    = "https://gitlab.com/"
	defaultConfigName = "default"
)

type NamespaceConfig struct {
	Labels []*Label
}

type Label struct {
	Name        string
	Color       string
	Description string
}

var (
	configFile = flag.String("f", "./labels.yaml", "Path to configuration file")
	dryRun     = flag.Bool("dry-run", false, "Runs without any changes in GitLab")
)

func groupLabelsMap(client *gitlab.Client, gid string) (map[string]*gitlab.GroupLabel, error) {
	page := 1
	groupLabels := make(map[string]*gitlab.GroupLabel)
	for {
		options := &gitlab.ListGroupLabelsOptions{
			Page: page,
		}
		labels, r, err := client.GroupLabels.ListGroupLabels(gid, options, nil)
		if err != nil {
			return nil, err
		}
		for _, label := range labels {
			groupLabels[label.Name] = label
		}
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return groupLabels, nil
}

func main() {
	flag.Parse()

	token := os.Getenv("GITLAB_TOKEN")

	endpoint := os.Getenv("GITLAB_ENDPOINT")
	if len(endpoint) == 0 {
		endpoint = defaultBaseURL
	}

	client, err := gitlab.NewClient(token,
		gitlab.WithBaseURL(endpoint),
	)
	if err != nil {
		logrus.Fatal(err)
	}

	namespaceConfigs := make(map[string]*NamespaceConfig)
	b, err := ioutil.ReadFile(*configFile)
	if err != nil {
		logrus.Fatal(err)
	}
	err = yaml.Unmarshal(b, &namespaceConfigs)
	if err != nil {
		logrus.Fatal(err)
	}

	labels := []*Label{}
	if defaultConfig, ok := namespaceConfigs[defaultConfigName]; ok {
		labels = append(labels, defaultConfig.Labels...)
	}

	for ns, config := range namespaceConfigs {
		if strings.HasPrefix(ns, ".") || ns == defaultConfigName {
			continue
		}
		logrus.Infof("Checking %s namespace...", ns)
		currentLabelsMap, err := groupLabelsMap(client, ns)
		if err != nil {
			logrus.Error(err)
			continue
		}
		configLabels := append(labels, config.Labels...)
		for _, label := range configLabels {
			if l, ok := currentLabelsMap[label.Name]; ok {
				if l.Color == label.Color && l.Description == label.Description {
					continue
				}
				logrus.Infof("Updating %s label...", label.Name)
				opts := &gitlab.UpdateGroupLabelOptions{
					Name:        gitlab.String(label.Name),
					Color:       gitlab.String(label.Color),
					Description: gitlab.String(label.Description),
				}
				if *dryRun {
					continue
				}
				_, _, err := client.GroupLabels.UpdateGroupLabel(ns, opts)
				if err != nil {
					logrus.Error(err)
				}
				continue
			}
			logrus.Infof("Creating %s label...", label.Name)
			opts := &gitlab.CreateGroupLabelOptions{
				Name:        gitlab.String(label.Name),
				Color:       gitlab.String(label.Color),
				Description: gitlab.String(label.Description),
			}
			if *dryRun {
				continue
			}
			_, _, err := client.GroupLabels.CreateGroupLabel(ns, opts)
			if err != nil {
				logrus.Error(err)
			}
		}
	}
}
