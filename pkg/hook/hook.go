package hook

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

type Hook struct {
	Plugins     *plugins.PluginClient
	SecretToken string

	wg sync.WaitGroup
}

func (h *Hook) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := h.validateWebhook(r)
	if err != nil {
		logrus.Error(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	payload, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logrus.WithError(err).Error("Failed to read request body")
		http.Error(w, "Failed to read request body.", http.StatusInternalServerError)
		return
	}

	eventType := gitlab.WebhookEventType(r)
	event, err := gitlab.ParseWebhook(eventType, payload)
	if err != nil {
		logrus.WithError(err).Error("Failed to parse webhook")
		http.Error(w, "Failed to parse webhook.", http.StatusInternalServerError)
		return
	}

	fmt.Fprint(w, "Event received. Have a nice day.")
	logrus.WithField("event_type", eventType).Debugf("Body: %v", event)

	if err := h.demuxEvent(eventType, event); err != nil {
		logrus.WithError(err).Error("Error parsing event.")
	}
}

func (h *Hook) validateWebhook(r *http.Request) error {
	if r.Header.Get("X-Gitlab-Event") == "" {
		return errors.New("Incorrect request: check X-Gitlab-Event")
	}
	if r.Header.Get("X-Gitlab-Token") != h.SecretToken {
		return errors.New("Incorrect request: check X-Gitlab-Token")
	}
	return nil
}

func (h *Hook) demuxEvent(eventType gitlab.EventType, event interface{}) error {
	var (
		projectID      int
		commitSHA      string
		mergeRequestID int
		username       string
		state          string
	)

	l := logrus.WithFields(
		logrus.Fields{
			"event-type": eventType,
		},
	)

	// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#events
	switch event := event.(type) {
	case *gitlab.MergeEvent:
		projectID = event.Project.ID
		commitSHA = event.ObjectAttributes.LastCommit.ID
		mergeRequestID = event.ObjectAttributes.IID
		username = event.User.Username
		state = event.ObjectAttributes.State
	case *gitlab.MergeCommentEvent:
		projectID = event.ProjectID
		commitSHA = event.MergeRequest.LastCommit.ID
		mergeRequestID = event.MergeRequest.IID
		username = event.User.Username
		state = event.MergeRequest.State
	}

	if mergeRequestID > 0 && username != h.Plugins.GitLabUser.Username &&
		state != "closed" && state != "merged" && h.Plugins.Config.ReportBuildStatus {
		h.handleCommitBuildStatusPending(l, projectID, mergeRequestID, commitSHA)
	}

	switch event := event.(type) {
	case *gitlab.MergeEvent:
		h.wg.Add(1)
		h.handleMergeRequestEvent(l, event)
	case *gitlab.MergeCommentEvent:
		h.wg.Add(1)
		h.handleMergeCommentEvent(l, event)
	case *gitlab.IssueEvent:
		h.wg.Add(1)
		h.handleIssueEvent(l, event)
	case *gitlab.IssueCommentEvent:
		h.wg.Add(1)
		h.handleIssueCommentEvent(l, event)
	case *gitlab.PipelineEvent:
		h.wg.Add(1)
		h.handlePipelineEvent(l, event)
	}

	if mergeRequestID > 0 && username != h.Plugins.GitLabUser.Username &&
		state != "closed" && state != "merged" && h.Plugins.Config.ReportBuildStatus {
		h.handleCommitBuildStatusDone(l, projectID, mergeRequestID, commitSHA)
	}

	return nil
}
