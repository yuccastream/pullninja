package utils

import (
	"fmt"
	"strings"

	"github.com/xanzy/go-gitlab"
)

func IsMergeRequestReviewedBy(mr *gitlab.MergeRequest, reviewer string) bool {
	for _, label := range mr.Labels {
		reviewer = strings.TrimPrefix(reviewer, "@")
		if label == fmt.Sprintf("review-needed/%s", reviewer) {
			return true
		}
		if label == fmt.Sprintf("review-ok/%s", reviewer) {
			return true
		}
	}
	return false
}
