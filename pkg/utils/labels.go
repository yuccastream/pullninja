package utils

func IsSimilarLabels(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for _, labelA := range a {
		found := false
		for _, labelB := range b {
			if labelA == labelB {
				found = true
				break
			}
		}
		if !found {
			return false
		}
	}
	return true
}
