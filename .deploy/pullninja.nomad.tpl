job "pullninja" {
  region      = "hetzner-fin"
  datacenters = ["infra"]
  namespace   = "production"
  type        = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

# шедулим только на эти ноды, на ноде infra-2 живёт тестинг, если там какие то траблы, остальные сервисы не должны страдать.
  constraint {
    attribute = "${node.unique.name}"
    operator  = "regexp"
    value     = "^infra-(0|1)$"
  }

  group "pullninja" {
    count = 1

    update {
      max_parallel      = 1
      health_check      = "checks"
      min_healthy_time  = "10s"
      healthy_deadline  = "1m"
      progress_deadline = "5m"
      auto_revert       = true
      auto_promote      = true
      canary            = 1
      stagger           = "30s"
    }

    reschedule {
      delay          = "10s"
      delay_function = "exponential"
      max_delay      = "2m"
      unlimited      = true
    }

    network {
      port "pullninja" {
        to = 8080
      }
    }

    task "pullninja" {
      driver = "docker"

      resources {
        cpu    = 100
        memory = 100
      }

      service {
        name = "pullninja"
        port = "pullninja"

        tags = [
          "traefik.enable=true",
          "traefik.http.routers.pullninja-web.rule=Host(`gitlab.yuccastream.com`) && Path(`/pullninja`)",
          "traefik.http.routers.pullninja-web.entryPoints=web",
          "traefik.http.routers.pullninja-websecure.rule=Host(`gitlab.yuccastream.com`) && Path(`/pullninja`)",
          "traefik.http.routers.pullninja-websecure.entryPoints=websecure",
          "traefik.http.routers.pullninja-web.middlewares=http-to-https@file",
          "traefik.http.routers.pullninja-websecure.tls=true",
          "traefik.http.routers.pullninja-websecure.tls.certResolver=letsDNS",
          "traefik.http.routers.pullninja-websecure.tls.domains[0].main=yucca.app",
          "traefik.http.routers.pullninja-websecure.tls.domains[0].sans=*.yucca.app,yuccastream.com,*.yuccastream.com",
        ]

        check {
          name     = "health"
          type     = "tcp"
          port     = "pullninja"
          interval = "30s"
          timeout  = "10s"
          check_restart {
            limit =3
            grace = "60s"
            ignore_warnings = false
          }
        }
      }
      env {
        GITLAB_TOKEN = "${PATCH_GITLAB_TOKEN}"
      }
      config {
        image = "${PATCH_IMAGE}"
        ports = ["pullninja"]
        args = [
          "-config",
          "/local/plugins.yaml",
        ]
      }
      template {
        destination = "local/plugins.yaml"
        change_mode = "restart"
        data = "{{ key \"gitlab/pullninja/plugins.yaml\" }}"
      }
    }
  }
}
