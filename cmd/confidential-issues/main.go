package main

import (
	"flag"
	"os"
	"strconv"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const (
	defaultBaseURL = "https://gitlab.com/"
)

var (
	project = flag.String("project", "", "Project path")
	state   = flag.String("state", "closed", "State of merge request")
	dryRun  = flag.Bool("dry-run", false, "Runs without any changes in GitLab")
)

func main() {
	flag.Parse()

	token := os.Getenv("GITLAB_TOKEN")

	endpoint := os.Getenv("GITLAB_ENDPOINT")
	if len(endpoint) == 0 {
		endpoint = defaultBaseURL
	}

	if len(*project) == 0 {
		logrus.Fatal("Project must be specified")
	}

	client, err := gitlab.NewClient(token,
		gitlab.WithBaseURL(endpoint),
	)
	if err != nil {
		logrus.Fatal(err)
	}

	issues, err := getProjectIssues(client, *project, *state)
	if err != nil {
		logrus.Fatal(err)
	}
	for _, issue := range issues {
		if issue.Confidential {
			continue
		}
		logrus.WithField("issue_id", issue.IID).Infof("Issue %q: Sets confidential=true", issue.Title)
		if *dryRun {
			continue
		}
		opts := &gitlab.UpdateIssueOptions{
			Confidential: gitlab.Bool(true),
		}
		_, _, err := client.Issues.UpdateIssue(*project, issue.IID, opts)
		if err != nil {
			logrus.Errorf("Failed to update %d issue: %v", issue.IID, err)
		}
	}
}

func getProjectIssues(gl *gitlab.Client, project string, state string) ([]*gitlab.Issue, error) {
	var issues []*gitlab.Issue
	logrus.Debugf("ListProjectIssues(%s)", project)
	page := 1
	options := &gitlab.ListProjectIssuesOptions{
		ListOptions: gitlab.ListOptions{
			Page: page,
		},
		State: gitlab.String(state),
	}
	for {
		options.ListOptions.Page = page
		items, r, err := gl.Issues.ListProjectIssues(project, options, nil)
		if err != nil {
			return nil, err
		}
		issues = append(issues, items...)
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return issues, nil
}
