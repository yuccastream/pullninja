package utils

import "testing"

func TestIsSimilarLabels(t *testing.T) {
	tests := []struct {
		a, b []string
		eq   bool
	}{
		{
			a:  []string{"a", "b"},
			b:  []string{"b", "a"},
			eq: true,
		},
		{
			a:  []string{"a"},
			b:  []string{"b", "a"},
			eq: false,
		},
		{
			a:  []string{"a", "b"},
			b:  []string{"c", "d"},
			eq: false,
		},
	}
	for id, test := range tests {
		eq := IsSimilarLabels(test.a, test.b)
		if eq != test.eq {
			t.Errorf("%d. Must be %v, but got %v", id, test.eq, eq)
		}
	}
}
