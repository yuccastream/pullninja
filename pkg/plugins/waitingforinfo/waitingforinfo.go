package waitingforinfo

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName          = "waitingforinfo"
	waitingForInfoLabel = "WaitingForInfo"
)

func init() {
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}

	var (
		containsWaitingForInfoLabel bool
		issueUpdateOptions          *gitlab.UpdateIssueOptions
		// Все лейблы Issue без WaitingForInfo
		labels []string
	)

	for _, label := range i.Labels {
		if label.Name == waitingForInfoLabel {
			containsWaitingForInfoLabel = true
		} else {
			labels = append(labels, label.Name)
		}
	}

	if len(i.ObjectAttributes.Description) > 0 && containsWaitingForInfoLabel {
		gitlabLabels := gitlab.Labels(labels)
		issueUpdateOptions = &gitlab.UpdateIssueOptions{
			Labels: &gitlabLabels,
		}
	} else if len(i.ObjectAttributes.Description) == 0 && !containsWaitingForInfoLabel {
		gitlabLabels := gitlab.Labels(append(labels, waitingForInfoLabel))
		issueUpdateOptions = &gitlab.UpdateIssueOptions{
			Labels: &gitlabLabels,
		}
	}

	if issueUpdateOptions == nil {
		return nil
	}

	_, _, err := pc.GitLabClient.Issues.UpdateIssue(i.Project.ID, i.ObjectAttributes.IID, issueUpdateOptions, nil)
	return err
}
