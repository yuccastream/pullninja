package hook

import (
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/buildstatuses"
	"gitlab.com/yuccastream/pullninja/pkg/labels"
)

func (h *Hook) handleMergeRequestEvent(l *logrus.Entry, mr *gitlab.MergeEvent) {
	defer h.wg.Done()
	l = l.WithFields(logrus.Fields{
		"project": mr.Project.Name,
		"repo":    mr.Repository.Name,
		"mr":      mr.ObjectAttributes.IID,
		"author":  mr.User.Name,
	})
	l.Infof("Merge request %s.", mr.ObjectAttributes.Title)
	for name, handler := range h.Plugins.MergeRequestHandlers(mr.Project.PathWithNamespace) {
		pc := h.Plugins
		l.WithField("plugin", name).Debug("Executing...")
		if err := handler(pc, mr); err != nil {
			l.WithField("plugin", name).WithError(err).Error("Error handling MergeRequestEvent.")
		}
	}
}

func (h *Hook) handleMergeCommentEvent(l *logrus.Entry, mc *gitlab.MergeCommentEvent) {
	defer h.wg.Done()
	l = l.WithFields(logrus.Fields{
		"project": mc.Project.Name,
		"repo":    mc.Repository.Name,
		"mr":      mc.MergeRequest.IID,
		"mc":      mc.ObjectAttributes.ID,
		"author":  mc.User.Name,
	})
	l.Infof("Merge comment %s.", mc.MergeRequest.Title)
	for name, handler := range h.Plugins.MergeCommentHandlers(mc.Project.PathWithNamespace) {
		pc := h.Plugins
		l.WithField("plugin", name).Debug("Executing...")
		if err := handler(pc, mc); err != nil {
			l.WithField("plugin", name).WithError(err).Error("Error handling MergeCommentEvent.")
		}
	}
}

func (h *Hook) handleIssueEvent(l *logrus.Entry, i *gitlab.IssueEvent) {
	defer h.wg.Done()
	l = l.WithFields(logrus.Fields{
		"project": i.Project.Name,
		"repo":    i.Repository.Name,
		"issue":   i.ObjectAttributes.ID,
		"author":  i.User.Name,
	})
	l.Infof("Issue %s.", i.ObjectAttributes.Title)
	for name, handler := range h.Plugins.IssueHandlers(i.Project.PathWithNamespace) {
		pc := h.Plugins
		l.WithField("plugin", name).Debug("Executing...")
		if err := handler(pc, i); err != nil {
			l.WithField("plugin", name).WithError(err).Error("Error handling IssueHandlers.")
		}
	}
}

func (h *Hook) handleIssueCommentEvent(l *logrus.Entry, ic *gitlab.IssueCommentEvent) {
	defer h.wg.Done()
	l = l.WithFields(logrus.Fields{
		"project": ic.Project.Name,
		"repo":    ic.Repository.Name,
		"issue":   ic.Issue.IID,
		"ic":      ic.ObjectAttributes.ID,
		"author":  ic.User.Name,
	})
	l.Infof("Issue comment %s.", ic.ObjectAttributes.Note)
	for name, handler := range h.Plugins.IssueCommentHandlers(ic.Project.PathWithNamespace) {
		pc := h.Plugins
		l.WithField("plugin", name).Debug("Executing...")
		if err := handler(pc, ic); err != nil {
			l.WithField("plugin", name).WithError(err).Error("Error handling IssueCommentHandlers.")
		}
	}
}

func (h *Hook) handlePipelineEvent(l *logrus.Entry, p *gitlab.PipelineEvent) {
	defer h.wg.Done()
	l = l.WithFields(logrus.Fields{
		"project":  p.Project.Name,
		"pipeline": p.ObjectAttributes.ID,
		"author":   p.User.Name,
	})
	l.Infof("Pipeline %d.", p.ObjectAttributes.ID)
	for name, handler := range h.Plugins.PipelineHandlers(p.Project.PathWithNamespace) {
		pc := h.Plugins
		l.WithField("plugin", name).Debug("Executing...")
		if err := handler(pc, p); err != nil {
			l.WithField("plugin", name).WithError(err).Error("Error handling IssueCommentHandlers.")
		}
	}
}

func (h *Hook) handleCommitBuildStatusPending(l *logrus.Entry, prID, mrID int, sha string) {
	l = l.WithFields(logrus.Fields{
		"pid": prID,
		"mr":  mrID,
		"sha": sha,
	})
	state := gitlab.Pending
	options := &gitlab.SetCommitStatusOptions{
		State:       state,
		Name:        gitlab.String(buildstatuses.ReviewNeededName),
		Description: gitlab.String(buildstatuses.ReviewNeededDescription),
	}
	_, _, err := h.Plugins.GitLabClient.Commits.SetCommitStatus(prID, sha, options, nil)
	if err != nil {
		l.WithError(err).Error("Error handling CommitBuildStatusPending.")
	}
}

func (h *Hook) handleCommitBuildStatusDone(l *logrus.Entry, prID, mrID int, sha string) {
	l = l.WithFields(logrus.Fields{
		"pid": prID,
		"mr":  mrID,
		"sha": sha,
	})
	mrService := h.Plugins.GitLabClient.MergeRequests
	m, _, err := mrService.GetMergeRequest(prID, mrID, nil, nil)
	if err != nil {
		l.WithError(err).Error("Error handling CommitBuildStatus.")
		return
	}
	hasUnreviewedUsers := false
	for _, label := range m.Labels {
		if strings.HasPrefix(label, labels.ReviewNeededPrefix) {
			hasUnreviewedUsers = true
			break
		}
	}
	state := gitlab.Success
	if hasUnreviewedUsers {
		state = gitlab.Failed
	}
	options := &gitlab.SetCommitStatusOptions{
		State:       state,
		Name:        gitlab.String(buildstatuses.ReviewNeededName),
		TargetURL:   gitlab.String(buildstatuses.ReviewNeededURL),
		Description: gitlab.String(buildstatuses.ReviewNeededDescription),
	}
	_, _, err = h.Plugins.GitLabClient.Commits.SetCommitStatus(prID, sha, options, nil)
	if err != nil {
		l.WithError(err).Error("Error handling CommitBuildStatus.")
	}
}
