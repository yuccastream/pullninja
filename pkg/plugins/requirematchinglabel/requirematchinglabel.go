package requirematchinglabel

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName = "requirematchinglabel"
)

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	labels := GetRequiredLabels(pc.Config.RequireMatchingLabel, mr.Project.PathWithNamespace, true, false)
	if len(labels) == 0 {
		return nil
	}
	mrService := pc.GitLabClient.MergeRequests
	m, _, err := mrService.GetMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}
	labelsNew := CreateLabelSet(labels, m.Labels)
	if utils.IsSimilarLabels(labelsNew, m.Labels) {
		return nil
	}
	gitlabLabels := gitlab.Labels(labelsNew)
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		Labels: &gitlabLabels,
	}
	_, _, err = mrService.UpdateMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, mergeUpdateOptions, nil)
	return err
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}
	labels := GetRequiredLabels(pc.Config.RequireMatchingLabel, i.Project.PathWithNamespace, false, true)
	if len(labels) == 0 {
		return nil
	}
	iService := pc.GitLabClient.Issues
	issue, _, err := iService.GetIssue(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}
	labelsNew := CreateLabelSet(labels, issue.Labels)
	if utils.IsSimilarLabels(labelsNew, issue.Labels) {
		return nil
	}
	gitlabLabels := gitlab.Labels(labelsNew)
	issueUpdateOptions := &gitlab.UpdateIssueOptions{
		Labels: &gitlabLabels,
	}
	_, _, err = iService.UpdateIssue(issue.ProjectID, i.ObjectAttributes.IID, issueUpdateOptions, nil)
	return err
}

func CreateLabelSet(reqLabels []plugins.RequireMatchingLabel, labels []string) []string {
	result := []string{}
	toAdd := labels
	toDel := []string{}
	for _, missedLabel := range reqLabels {
		match := false
		for _, label := range labels {
			if missedLabel.Re.MatchString(label) {
				match = true
				break
			}
		}
		if !match {
			toAdd = append(toAdd, missedLabel.MissingLabel)
		} else {
			toDel = append(toDel, missedLabel.MissingLabel)
		}
	}
	toAdd = utils.RemoveDuplicatesUnordered(toAdd)
	for _, label := range toAdd {
		toDelete := false
		for _, delLabel := range toDel {
			if label == delLabel {
				toDelete = true
				break
			}
		}
		if !toDelete {
			result = append(result, label)
		}
	}
	return result
}

func GetRequiredLabels(labels []plugins.RequireMatchingLabel, pathWithNamespace string, mr bool, issue bool) []plugins.RequireMatchingLabel {
	addedLabels := make(map[string]bool)
	result := []plugins.RequireMatchingLabel{}
	namespaces := plugins.GetNamespaceParts(pathWithNamespace)
	for _, reqLabel := range labels {
		if (mr && !reqLabel.MRs) || (issue && !reqLabel.Issues) {
			continue
		}
		for _, ns := range namespaces {
			if _, ok := addedLabels[reqLabel.MissingLabel]; ok {
				continue
			}
			if reqLabel.Namespace == ns {
				addedLabels[reqLabel.MissingLabel] = true
				result = append(result, reqLabel)
			}
		}
	}
	return result
}
