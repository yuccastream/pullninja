package welcome

import (
	"bytes"
	"html/template"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName                    = "welcome"
	defaultWelcomeMessageTemplate = "Добро пожаловать, @{{.Author.Username}} и спасибо за твой первый Merge Request! :smiley:"
)

type PRInfo struct {
	Author  *gitlab.User
	Project interface{}
	Repo    *gitlab.Repository
}

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}

	if mr.ObjectAttributes.Action != "open" {
		return nil
	}

	opts := &gitlab.ListProjectMergeRequestsOptions{}
	mergeRequests, _, err := pc.GitLabClient.MergeRequests.ListProjectMergeRequests(mr.Project.ID, opts)
	if err != nil {
		return err
	}

	welcomeTemplate := pc.Config.WelcomeMessageTemplate
	if len(welcomeTemplate) == 0 {
		welcomeTemplate = defaultWelcomeMessageTemplate
	}

	if len(mergeRequests) == 0 || len(mergeRequests) == 1 && mergeRequests[0].IID == mr.ObjectAttributes.IID {
		parsedTemplate, err := template.New("welcome").Parse(welcomeTemplate)
		if err != nil {
			return err
		}

		var msgBuffer bytes.Buffer
		err = parsedTemplate.Execute(&msgBuffer, PRInfo{
			Author:  mr.User,
			Project: mr.Project,
			Repo:    mr.Repository,
		})
		if err != nil {
			return err
		}

		opts := &gitlab.CreateMergeRequestNoteOptions{
			Body: gitlab.String(msgBuffer.String()),
		}
		_, _, err = pc.GitLabClient.Notes.CreateMergeRequestNote(mr.Project.ID, mr.ObjectAttributes.IID, opts)
		return err
	}

	return nil
}
