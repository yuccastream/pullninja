package defaultmilestone

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const pluginName = "defaultmilestone"

func init() {
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}
	if i.ObjectAttributes.Action != "open" {
		return nil
	}
	if i.ObjectAttributes.MilestoneID != 0 {
		return nil
	}
	if pc.Config.DefaultMilestoneID == 0 {
		return nil
	}
	issueUpdateOptions := &gitlab.UpdateIssueOptions{
		MilestoneID: gitlab.Int(pc.Config.DefaultMilestoneID),
	}
	_, _, err := pc.GitLabClient.Issues.UpdateIssue(
		i.ObjectAttributes.ProjectID,
		i.ObjectAttributes.IID,
		issueUpdateOptions,
		nil,
	)
	return err
}
