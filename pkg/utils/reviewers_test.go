package utils

import (
	"testing"

	"github.com/xanzy/go-gitlab"
)

func TestIsMergeRequestReviewedBy(t *testing.T) {
	tests := []struct {
		mr       *gitlab.MergeRequest
		reviewer string
		reviewed bool
	}{
		{
			mr:       &gitlab.MergeRequest{},
			reviewer: "foobar",
			reviewed: false,
		},
		{
			mr: &gitlab.MergeRequest{
				Labels: []string{"review-ok/foobar"},
			},
			reviewer: "foobar",
			reviewed: true,
		},
		{
			mr: &gitlab.MergeRequest{
				Labels: []string{"review-needed/foobar"},
			},
			reviewer: "foobar",
			reviewed: true,
		},
	}
	for id, test := range tests {
		rv := IsMergeRequestReviewedBy(test.mr, test.reviewer)
		if test.reviewed != rv {
			t.Errorf("%d. Must be %v, but got %v", id, test.reviewed, rv)
		}
	}
}
