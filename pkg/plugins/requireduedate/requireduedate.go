package requireduedate

import (
	"time"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName                 = "requireduedate"
	defaultRequireDueDateLabel = "needs-due-date"
)

func init() {
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func AddNonexistentLabel(labelSet []string, label string) ([]string, bool) {
	for _, originalLabel := range labelSet {
		if originalLabel == label {
			return labelSet, false
		}
	}
	labelSet = append(labelSet, label)
	labelSet = utils.RemoveDuplicatesUnordered(labelSet)
	return labelSet, true
}

func DelExistentLabel(labelSet []string, label string) ([]string, bool) {
	var (
		result  []string
		changed bool
	)
	for _, originalLabel := range labelSet {
		if originalLabel == label {
			changed = true
			continue
		}
		result = append(result, originalLabel)
	}
	result = utils.RemoveDuplicatesUnordered(result)
	return result, changed
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}

	issue, _, err := pc.GitLabClient.Issues.GetIssue(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}

	var (
		labelSet []string
		changed  bool
	)

	dueDateLabel := pc.Config.RequireDueDateLabel
	if len(dueDateLabel) == 0 {
		dueDateLabel = defaultRequireDueDateLabel
	}

	if issue.DueDate == nil || time.Time(*issue.DueDate).IsZero() {
		labelSet, changed = AddNonexistentLabel(issue.Labels, dueDateLabel)
	} else {
		labelSet, changed = DelExistentLabel(issue.Labels, dueDateLabel)
	}

	if !changed || utils.IsSimilarLabels(labelSet, issue.Labels) {
		return nil
	}

	gitlabLabels := gitlab.Labels(labelSet)
	issueUpdateOptions := &gitlab.UpdateIssueOptions{
		Labels: &gitlabLabels,
	}

	_, _, err = pc.GitLabClient.Issues.UpdateIssue(issue.ProjectID, i.ObjectAttributes.IID, issueUpdateOptions, nil)
	return err
}
