package hook

import (
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/areas"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/assign"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/assignee"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/autoconfidentiality"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/declinepipeline"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/defaultmilestone"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/deletesrcbranch"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/epiclink"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/freeze"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/incoming"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/lgtm"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/milestone"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/owners"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/requireduedate"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/requirematchinglabel"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/size"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/waitingforinfo"
	_ "gitlab.com/yuccastream/pullninja/pkg/plugins/welcome"
)
