package plugins

import (
	"fmt"
	"regexp"
)

type Config struct {
	// Enabled plugings by Project.PathWithNamespace
	// Example: yuccastream / infrastructure / qa-prom
	// This will be transcripted as 4 items:
	// - yuccastream/infrastructure/qa-prom
	// - yuccastream
	// - infrastructure
	// - qa-prom
	Plugins map[string][]string `yaml:"plugins,omitempty"`
	// Allow report build status to merge request GitLab
	// CI pipeline. It may be useful to protect master-branch
	// from unreviewed changes.
	ReportBuildStatus         bool                   `yaml:"report_build_status"`
	RequireMatchingLabel      []RequireMatchingLabel `yaml:"require_matching_label"`
	AutoConfidentialityUsers  []string               `yaml:"autoconfidentiality_users"`
	AutoConfidentialityLabels []string               `yaml:"autoconfidentiality_labels"`
	DefaultMilestoneID        int                    `yaml:"default_milestone_id"`
	RequireDueDateLabel       string                 `yaml:"require_due_date_label"`
	WelcomeMessageTemplate    string                 `yaml:"welcome_message_template"`
	IncomingLabels            []IncomingLabel        `yaml:"incoming_labels"`
}

type IncomingLabel struct {
	Source string         `yaml:"source"`
	Regexp string         `yaml:"regexp,omitempty"`
	Re     *regexp.Regexp `yaml:"-"`
	Label  string         `yaml:"label"`
}

type RequireMatchingLabel struct {
	Namespace    string         `yaml:"ns,omitempty"`
	MRs          bool           `yaml:"mrs,omitempty"`
	Issues       bool           `yaml:"issues,omitempty"`
	Regexp       string         `yaml:"regexp,omitempty"`
	Re           *regexp.Regexp `yaml:"-"`
	MissingLabel string         `yaml:"missing_label,omitempty"`
}

func (c *Config) Process() error {
	for id, label := range c.RequireMatchingLabel {
		if len(label.Regexp) == 0 {
			return fmt.Errorf("regexp RequireMatchingLabel %d must be specified", id)
		}
		re, err := regexp.Compile(label.Regexp)
		if err != nil {
			return fmt.Errorf("regexp RequireMatchingLabel %d error: %v", id, err)
		}
		c.RequireMatchingLabel[id].Re = re
	}
	for id, label := range c.IncomingLabels {
		if len(label.Regexp) == 0 {
			return fmt.Errorf("regexp IncomingLabel %d must be specified", id)
		}
		re, err := regexp.Compile(label.Regexp)
		if err != nil {
			return fmt.Errorf("regexp IncomingLabel %d error: %v", id, err)
		}
		c.IncomingLabels[id].Re = re
	}
	return nil
}
