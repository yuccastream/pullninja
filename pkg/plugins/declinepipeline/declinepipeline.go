package declinepipeline

import (
	"errors"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName = "declinepipeline"
)

func init() {
	plugins.RegisterPipelineHandler(pluginName, handlePipelineEvent)
}

func handlePipelineEvent(pc *plugins.PluginClient, p *gitlab.PipelineEvent) error {
	var (
		failed bool
		match  bool
	)
	if pc.GitLabUser.Username == p.User.Username {
		return nil
	}
	opts := &gitlab.ListProjectPipelinesOptions{
		Ref:    gitlab.String(p.ObjectAttributes.Ref),
		Status: gitlab.BuildState(gitlab.Running),
	}
	pls, _, err := pc.GitLabClient.Pipelines.ListProjectPipelines(p.Project.ID, opts)
	if err != nil {
		return err
	}
	for _, pl := range pls {
		if pl.ID == p.ObjectAttributes.ID {
			match = true
			continue
		}
		if !match {
			continue
		}
		plr, _, err := pc.GitLabClient.Pipelines.GetPipeline(p.Project.ID, pl.ID)
		if err != nil {
			logrus.Warnf("Failed to get pipeline %d: %v", pl.ID, err)
			continue
		}
		if pc.GitLabUser.Username == plr.User.Username {
			continue
		}
		_, _, err = pc.GitLabClient.Pipelines.CancelPipelineBuild(p.Project.ID, pl.ID)
		if err != nil {
			failed = true
			logrus.Warnf("Failed to cancel pipeline %d: %v", pl.ID, err)
		}
	}
	if failed {
		return errors.New("failed to cancel pipeline(s)")
	}
	return nil
}
