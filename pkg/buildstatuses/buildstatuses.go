package buildstatuses

const (
	FreezeName        = "pullninja/frozen"
	FreezeDescription = "Merge Request frozen"

	ReviewNeededName        = "pullninja/review-needed"
	ReviewNeededDescription = "Check reviewers"
	ReviewNeededURL         = "https://gitlab.com/cheap/pullninja/blob/master/build-status/review-needed.md"
)
