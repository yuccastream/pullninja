package main

import (
	"flag"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gopkg.in/yaml.v2"

	"gitlab.com/yuccastream/pullninja/pkg/hook"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	defaultBaseURL = "https://gitlab.com/"
)

var (
	listenAddress = flag.String("listen-address", ":8080", "Address to serve HTTP requests")
	configFile    = flag.String("config", "plugins.yaml", "Path to configuration file")
	logLevel      = flag.String("log-level", "info", "Level of logging")
)

func main() {
	flag.Parse()

	logrus.SetFormatter(&logrus.JSONFormatter{})
	if level, err := logrus.ParseLevel(*logLevel); err == nil {
		logrus.SetLevel(level)
	}

	token := os.Getenv("GITLAB_TOKEN")
	secretToken := os.Getenv("GITLAB_WEBHOOK_SECRET_TOKEN")
	endpoint := os.Getenv("GITLAB_ENDPOINT")
	if len(endpoint) == 0 {
		endpoint = defaultBaseURL
	}

	logrus.Info("Starting pullninja...")
	logrus.Infof("GitLab endpoint: %s", endpoint)

	config := &plugins.Config{}
	if len(*configFile) > 0 {
		b, err := ioutil.ReadFile(*configFile)
		if err != nil {
			logrus.Fatal(err)
		}
		err = yaml.Unmarshal(b, &config)
		if err != nil {
			logrus.Fatal(err)
		}
		if err := config.Process(); err != nil {
			logrus.Fatal(err)
		}
	} else {
		logrus.Warn("Starts without an plugins configuration")
	}

	client, err := gitlab.NewClient(token,
		gitlab.WithBaseURL(endpoint),
	)
	if err != nil {
		logrus.Fatal(err)
	}

	user, _, err := client.Users.CurrentUser(nil)
	if err != nil {
		logrus.Fatal(err)
	}

	pluginClient := &plugins.PluginClient{
		GitLabClient: client,
		GitLabUser:   user,
		Config:       config,
	}

	h := &hook.Hook{
		Plugins:     pluginClient,
		SecretToken: secretToken,
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {})
	http.Handle("/pullninja", h)

	logrus.Infof("Start listening %s address", *listenAddress)
	logrus.Fatal(http.ListenAndServe(*listenAddress, nil))
}
