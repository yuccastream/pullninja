package assign

import (
	"regexp"
	"strings"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/labels"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName = "assign"
)

var (
	CCRegexp = regexp.MustCompile(`(?mi)^/(un)?cc(( +@?[-/\w\.\"]+?)*)\s*$`)
)

func init() {
	plugins.RegisterMergeCommentHandler(pluginName, handleMergeCommentEvent)
}

func parseLogins(text string) []string {
	var parts []string
	for _, p := range strings.Split(text, " ") {
		t := strings.Trim(p, "@ ")
		if t == "" {
			continue
		}
		t = strings.Replace(t, "\"", "", -1)
		parts = append(parts, t)
	}
	return parts
}

func handleMergeCommentEvent(pc *plugins.PluginClient, mc *gitlab.MergeCommentEvent) error {
	if pc.GitLabUser.Username == mc.User.Username {
		return nil
	}
	matches := CCRegexp.FindAllStringSubmatch(mc.ObjectAttributes.Note, -1)
	if matches == nil {
		return nil
	}
	users := make(map[string]bool)
	for _, re := range matches {
		add := re[1] != "un"
		if re[2] == "" {
			users[mc.User.Username] = add
		} else {
			for _, login := range parseLogins(re[2]) {
				users[login] = add
			}
		}
	}
	mrService := pc.GitLabClient.MergeRequests
	mr, _, err := mrService.GetMergeRequest(mc.ProjectID, mc.MergeRequest.IID, nil, nil)
	if err != nil {
		return err
	}
	labelsNew := mr.Labels
	var toAdd, toRemove []string
	for login, add := range users {
		if add {
			toAdd = append(toAdd, login)
		} else {
			toRemove = append(toRemove, login)
		}
	}
	for _, login := range toAdd {
		if !utils.IsMergeRequestReviewedBy(mr, login) {
			labelsNew = append(labelsNew, labels.ReviewNeededPrefix+login)
		}
	}
	labelsSorted := []string{}
	for _, label := range labelsNew {
		delete := false
		for _, login := range toRemove {
			if (strings.HasPrefix(label, labels.ReviewNeededPrefix) || strings.HasPrefix(label, labels.ReviewOKPrefix)) && strings.HasSuffix(label, login) {
				delete = true
				break
			}
		}
		if !delete {
			labelsSorted = append(labelsSorted, label)
		}
	}
	labelsSorted = utils.RemoveDuplicatesUnordered(labelsSorted)
	if utils.IsSimilarLabels(labelsSorted, mr.Labels) {
		return nil
	}
	gitlabLabels := gitlab.Labels(labelsSorted)
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		Labels: &gitlabLabels,
	}
	_, _, err = mrService.UpdateMergeRequest(mc.ProjectID, mc.MergeRequest.IID, mergeUpdateOptions, nil)
	return err
}
