package incoming

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

func Test_labelsFromIssueEvent(t *testing.T) {
	pc := &plugins.PluginClient{
		Config: &plugins.Config{},
	}
	ev := &gitlab.IssueEvent{
		User: &gitlab.User{},
	}
	assert.Equal(t, []string{}, labelsFromIssueEvent(pc, ev))
	ev.User.Username = "support-bot"
	ev.ObjectAttributes.Action = "close"
	ev.ObjectAttributes.Title = "title"
	ev.ObjectAttributes.Description = "description"
	assert.Equal(t, []string{}, labelsFromIssueEvent(pc, ev))
	ev.User.Username = "support-bot"
	ev.ObjectAttributes.Action = "open"
	ev.ObjectAttributes.Title = "title"
	ev.ObjectAttributes.Description = "description"
	assert.Equal(t, []string{}, labelsFromIssueEvent(pc, ev))
	pc.Config.IncomingLabels = []plugins.IncomingLabel{
		{
			Re:     regexp.MustCompile(`.*`),
			Source: "title",
			Label:  "title",
		},
		{
			Re:     regexp.MustCompile(`.*`),
			Source: "title",
			Label:  "title",
		},
		{
			Re:     regexp.MustCompile(`.*`),
			Source: "description",
			Label:  "description",
		},
	}
	assert.Equal(t, 2, len(labelsFromIssueEvent(pc, ev)))
	pc.Config.IncomingLabels = []plugins.IncomingLabel{
		{
			Re:     regexp.MustCompile(`foobars.*`),
			Source: "title",
			Label:  "title",
		},
	}
	assert.Equal(t, 0, len(labelsFromIssueEvent(pc, ev)))
}
