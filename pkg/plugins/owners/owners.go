package owners

import (
	"strings"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/labels"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/repoowners"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName = "owners"
)

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	if mr.ObjectAttributes.State == "merged" || mr.ObjectAttributes.State == "closed" {
		return nil
	}
	rawFileOptions := &gitlab.GetRawFileOptions{
		Ref: gitlab.String(mr.ObjectAttributes.TargetBranch),
	}
	body, _, err := pc.GitLabClient.RepositoryFiles.GetRawFile(mr.Project.ID, "CODEOWNERS", rawFileOptions, nil)
	if err != nil {
		return err
	}
	owners, err := repoowners.Parse(body)
	if err != nil {
		return err
	}
	mrService := pc.GitLabClient.MergeRequests
	m, _, err := mrService.GetMergeRequestChanges(mr.Project.ID, mr.ObjectAttributes.IID, nil)
	if err != nil {
		return err
	}
	var affectedFiles []string
	for _, change := range m.Changes {
		affectedFiles = append(affectedFiles, change.OldPath)
		affectedFiles = append(affectedFiles, change.NewPath)
	}
	affectedFiles = utils.RemoveDuplicatesUnordered(affectedFiles)
	labelsNew := m.Labels
	for _, affectedFile := range affectedFiles {
		reviewers := owners.FindOwnersForFile(affectedFile)
		for _, reviewer := range reviewers {
			reviewer = strings.TrimPrefix(reviewer, "@")
			if utils.IsMergeRequestReviewedBy(m, reviewer) {
				continue
			}
			if reviewer == m.Author.Username {
				continue
			}
			labelsNew = append(labelsNew, labels.ReviewNeededPrefix+reviewer)
		}
	}
	labelsNew = utils.RemoveDuplicatesUnordered(labelsNew)
	if utils.IsSimilarLabels(labelsNew, m.Labels) {
		return nil
	}
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		Labels: &labelsNew,
	}
	_, _, err = mrService.UpdateMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, mergeUpdateOptions, nil)
	return err
}
