package deletesrcbranch

import (
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName = "deletesrcbranch"
)

func init() {
	plugins.RegisterMergeRequestHandler(pluginName, handleMergeRequestEvent)
}

func handleMergeRequestEvent(pc *plugins.PluginClient, mr *gitlab.MergeEvent) error {
	if pc.GitLabUser.Username == mr.User.Username {
		return nil
	}
	if mr.ObjectAttributes.Action != "open" {
		return nil
	}
	mrService := pc.GitLabClient.MergeRequests
	mergeUpdateOptions := &gitlab.UpdateMergeRequestOptions{
		RemoveSourceBranch: gitlab.Bool(true),
	}
	_, _, err := mrService.UpdateMergeRequest(mr.Project.ID, mr.ObjectAttributes.IID, mergeUpdateOptions, nil)
	return err
}
