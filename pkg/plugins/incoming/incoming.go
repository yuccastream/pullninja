package incoming

import (
	"strings"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

const (
	pluginName = "incoming"
)

func init() {
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func labelsFromIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) []string {
	if i.User.Username != "support-bot" {
		return []string{}
	}
	if i.ObjectAttributes.Action != "open" {
		return []string{}
	}
	var labels []string
	for _, label := range pc.Config.IncomingLabels {
		var text string
		switch strings.ToLower(label.Source) {
		case "title":
			text = i.ObjectAttributes.Title
		default:
			text = i.ObjectAttributes.Description
		}
		if label.Re.MatchString(text) {
			labels = append(labels, label.Label)
		}
	}
	labels = utils.RemoveDuplicatesUnordered(labels)
	return labels
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	labels := labelsFromIssueEvent(pc, i)
	if len(labels) == 0 {
		return nil
	}

	labelSet := gitlab.Labels(labels)
	issueUpdateOptions := &gitlab.UpdateIssueOptions{
		Labels: &labelSet,
	}
	_, _, err := pc.GitLabClient.Issues.UpdateIssue(i.Project.ID, i.ObjectAttributes.IID, issueUpdateOptions, nil)
	return err
}
