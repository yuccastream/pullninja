package repoowners

import (
	"bufio"
	"bytes"
	"strings"

	"github.com/cloudfoundry/cli/util/glob"

	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

type RepoOwners struct {
	rules []*OwnerRule
}

type OwnerRule struct {
	Mask   string
	Owners []string
}

func Parse(raw []byte) (*RepoOwners, error) {
	result := &RepoOwners{
		rules: []*OwnerRule{},
	}
	s := bufio.NewScanner(bytes.NewReader(raw))
	for line := 1; s.Scan(); line++ {
		ownerRuleRaw := strings.TrimSpace(s.Text())
		if len(ownerRuleRaw) == 0 {
			continue
		}
		if strings.HasPrefix(ownerRuleRaw, "#") {
			continue
		}
		ownerRuleArgs := strings.Split(ownerRuleRaw, " ")
		if len(ownerRuleArgs) == 1 {
			continue
		}
		owr := &OwnerRule{
			Mask:   ownerRuleArgs[0],
			Owners: ownerRuleArgs[1:],
		}
		for i, owner := range owr.Owners {
			owr.Owners[i] = strings.TrimPrefix(owner, "@")
		}
		result.rules = append(result.rules, owr)
	}
	return result, nil
}

func (r *RepoOwners) FindOwnersForFile(file string) []string {
	for _, rule := range r.rules {
		gl := glob.MustCompileGlob(rule.Mask)
		if gl.Match(file) {
			return rule.Owners
		}
	}
	return []string{}
}

func (r *RepoOwners) FindOwnersForFiles(files []string) []string {
	var ownersList []string
	for _, fileName := range files {
		ownersList = append(ownersList, r.FindOwnersForFile(fileName)...)
	}
	ownersList = utils.RemoveDuplicatesUnordered(ownersList)
	return ownersList
}
