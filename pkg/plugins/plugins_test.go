package plugins

import (
	"testing"

	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

func TestGetNamespaceParts(t *testing.T) {
	tests := []struct {
		ns    string
		parts []string
	}{
		{
			ns: "project",
			parts: []string{
				"project",
			},
		},
		{
			ns: "group/project",
			parts: []string{
				"group/project",
				"group",
				"project",
			},
		},
		{
			ns: "group/subgroup/project",
			parts: []string{
				"group/subgroup/project",
				"subgroup/project",
				"group/subgroup",
				"group",
				"subgroup",
				"project",
			},
		},
		{
			ns: "group/subgroupA/subgroupB/project",
			parts: []string{
				"group/subgroupA/subgroupB/project",
				"group/subgroupA/subgroupB",
				"group/subgroupA",
				"group",
				"subgroupA/subgroupB/project",
				"subgroupB/project",
				"project",
				"subgroupA",
				"subgroupB",
			},
		},
	}
	for id, test := range tests {
		resultParts := GetNamespaceParts(test.ns)
		if !utils.IsSimilarLabels(resultParts, test.parts) {
			t.Errorf("%d. Must be %v, but got %v", id, test.parts, resultParts)
		}
	}
}
