package freeze

import (
	"regexp"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/buildstatuses"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const (
	pluginName = "freeze"
)

var (
	freezeRe   = regexp.MustCompile(`(?mi)^/(freeze)\s*$`)
	unfreezeRe = regexp.MustCompile(`(?mi)^/(unfreeze)\s*$`)
)

func init() {
	plugins.RegisterMergeCommentHandler(pluginName, handleMergeCommentEvent)
}

func handleMergeCommentEvent(pc *plugins.PluginClient, mc *gitlab.MergeCommentEvent) error {
	if pc.GitLabUser.Username == mc.User.Username {
		return nil
	}

	var options *gitlab.SetCommitStatusOptions
	if freezeRe.MatchString(mc.ObjectAttributes.Note) {
		options = &gitlab.SetCommitStatusOptions{
			State:       gitlab.Failed,
			Name:        gitlab.String(buildstatuses.FreezeName),
			Description: gitlab.String(buildstatuses.FreezeDescription),
		}
	} else if unfreezeRe.MatchString(mc.ObjectAttributes.Note) {
		options = &gitlab.SetCommitStatusOptions{
			State:       gitlab.Success,
			Name:        gitlab.String(buildstatuses.FreezeName),
			Description: gitlab.String(buildstatuses.FreezeDescription),
		}
	} else {
		return nil
	}

	_, _, err := pc.GitLabClient.Commits.SetCommitStatus(mc.ProjectID, mc.MergeRequest.LastCommit.ID, options)
	return err
}
