package requirematchinglabel

import (
	"regexp"
	"strings"
	"testing"

	"gitlab.com/yuccastream/pullninja/pkg/plugins"
	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

func TestCreateLabelSet(t *testing.T) {
	configs := []plugins.RequireMatchingLabel{
		{
			Re:           regexp.MustCompile("^kind/"),
			MissingLabel: "needs-kind",
		},
		{
			Re:           regexp.MustCompile("^(foo|doo|faa)/"),
			MissingLabel: "needs-foo-doo-faa",
		},
	}
	tests := []struct {
		initialLabels []string
		result        []string
	}{
		{
			initialLabels: []string{},
			result:        []string{"needs-kind", "needs-foo-doo-faa"},
		},
		{
			initialLabels: []string{"foo-bar"},
			result:        []string{"needs-kind", "needs-foo-doo-faa", "foo-bar"},
		},
		{
			initialLabels: []string{"kind/feature", "needs-kind"},
			result:        []string{"kind/feature", "needs-foo-doo-faa"},
		},
		{
			initialLabels: []string{"kind/feature", "foo/bar"},
			result:        []string{"kind/feature", "foo/bar"},
		},
		{
			initialLabels: []string{"kind/feature", "foo/bar", "doo/bar", "needs-foo-doo-faa"},
			result:        []string{"kind/feature", "foo/bar", "doo/bar"},
		},
	}
	for id, test := range tests {
		labelsNew := CreateLabelSet(configs, test.initialLabels)
		if !utils.IsSimilarLabels(labelsNew, test.result) {
			t.Errorf("%d. Must be %s, but got %s", id, strings.Join(test.result, ", "), strings.Join(labelsNew, ", "))
		}
	}
}

func TestGetRequiredLabels(t *testing.T) {
	configs := []plugins.RequireMatchingLabel{
		{
			MissingLabel: "A",
			Namespace:    "groupA/subgroupA/projectA",
			Issues:       true,
		},
		{
			MissingLabel: "B",
			Namespace:    "groupA/subgroupA",
			MRs:          true,
		},
		{
			MissingLabel: "C",
			Namespace:    "groupA",
			MRs:          true,
			Issues:       true,
		},
		{
			MissingLabel: "D",
			Namespace:    "projectA",
			MRs:          true,
		},
	}
	tests := []struct {
		ns     string
		labels int
		issue  bool
		mr     bool
	}{
		{
			ns:     "projectB",
			labels: 0,
			issue:  true,
			mr:     false,
		},
		{
			ns:     "projectA",
			labels: 1,
			issue:  false,
			mr:     true,
		},
		{
			ns:     "groupA/subgroupA/projectA",
			labels: 2,
			issue:  true,
			mr:     false,
		},
	}
	for _, test := range tests {
		labels := GetRequiredLabels(configs, test.ns, test.mr, test.issue)
		if len(labels) != test.labels {
			t.Errorf("%s. Must be %d, but got %d", test.ns, test.labels, len(labels))
		}
	}
}
