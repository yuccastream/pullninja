FROM golang:1.16.3-stretch as build
WORKDIR /go/src/gitlab.com/yuccastream/pullninja/

COPY . ./
RUN ./build.sh bin

FROM debian:10-slim as release
LABEL io.yucca.pullninja.image=true
WORKDIR /opt/pullninja

RUN apt-get update \
    && apt-get install -y ca-certificates \
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean
COPY --from=build /go/src/gitlab.com/yuccastream/pullninja/pullninja /opt/pullninja/pullninja

EXPOSE 8080
ENTRYPOINT [ "/opt/pullninja/pullninja" ]
CMD [ "-h" ]
