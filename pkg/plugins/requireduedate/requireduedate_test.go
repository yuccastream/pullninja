package requireduedate

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAddNonexistentLabel(t *testing.T) {
	labelSet, changed := AddNonexistentLabel([]string{"foo", "bar"}, "foo")
	assert.False(t, changed)
	assert.Equal(t, 2, len(labelSet))

	labelSet, changed = AddNonexistentLabel([]string{"foo", "bar"}, "foobar")
	assert.True(t, changed)
	assert.Equal(t, 3, len(labelSet))
}

func TestDelExistentLabel(t *testing.T) {
	labelSet, changed := DelExistentLabel([]string{"foo", "bar"}, "foobar")
	assert.False(t, changed)
	assert.Equal(t, 2, len(labelSet))

	labelSet, changed = DelExistentLabel([]string{"foo", "bar"}, "foo")
	assert.True(t, changed)
	assert.Equal(t, 1, len(labelSet))
}
