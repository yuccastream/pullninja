package repoareas

import (
	"bufio"
	"bytes"
	"strings"

	"github.com/cloudfoundry/cli/util/glob"

	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

type RepoAreas struct {
	rules []*AreaRule
}

type AreaRule struct {
	Mask   string
	Labels []string
}

func Parse(raw []byte) (*RepoAreas, error) {
	result := &RepoAreas{
		rules: []*AreaRule{},
	}
	s := bufio.NewScanner(bytes.NewReader(raw))
	for line := 1; s.Scan(); line++ {
		areaRuleRaw := strings.TrimSpace(s.Text())
		if len(areaRuleRaw) == 0 {
			continue
		}
		if strings.HasPrefix(areaRuleRaw, "#") {
			continue
		}
		areaRuleArgs := strings.Split(areaRuleRaw, " ")
		if len(areaRuleArgs) == 1 {
			continue
		}
		owr := &AreaRule{
			Mask:   areaRuleArgs[0],
			Labels: areaRuleArgs[1:],
		}
		for i, labels := range owr.Labels {
			owr.Labels[i] = labels
		}
		result.rules = append(result.rules, owr)
	}
	return result, nil
}

func (r *RepoAreas) FindLabelsForFile(file string) []string {
	for _, rule := range r.rules {
		gl := glob.MustCompileGlob(rule.Mask)
		if gl.Match(file) {
			return rule.Labels
		}
	}
	return []string{}
}

func (r *RepoAreas) FindLabelsForFiles(files []string) []string {
	var labelsList []string
	for _, fileName := range files {
		labelsList = append(labelsList, r.FindLabelsForFile(fileName)...)
	}
	labelsList = utils.RemoveDuplicatesUnordered(labelsList)
	return labelsList
}
