package epiclink

import (
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/labels"
	"gitlab.com/yuccastream/pullninja/pkg/plugins"
)

const pluginName = "epiclink"

func init() {
	plugins.RegisterIssueHandler(pluginName, handleIssueEvent)
}

func handleIssueEvent(pc *plugins.PluginClient, i *gitlab.IssueEvent) error {
	var linkedEpics []string

	if pc.GitLabUser.Username == i.User.Username {
		return nil
	}

	issue, _, err := pc.GitLabClient.Issues.GetIssue(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID)
	if err != nil {
		return err
	}

	for _, label := range issue.Labels {
		if label == labels.KindEpic {
			return nil
		}
		if strings.HasPrefix(label, "epic::") {
			linkedEpics = append(linkedEpics, label)
		}
	}

	if len(linkedEpics) == 0 {
		return nil
	}

	for _, label := range linkedEpics {
		listOpts := &gitlab.ListProjectIssuesOptions{
			State:  gitlab.String("opened"),
			Labels: []string{labels.KindEpic, label},
		}
		issues, _, err := pc.GitLabClient.Issues.ListProjectIssues(i.ObjectAttributes.ProjectID, listOpts)
		if err != nil {
			logrus.WithError(err).Warnf("Failed to list issues: %s", strings.Join(listOpts.Labels, ", "))
			continue
		}

		for _, issue := range issues {
			pid := strconv.Itoa(issue.ProjectID)
			iid := strconv.Itoa(issue.IID)
			linkOpts := &gitlab.CreateIssueLinkOptions{
				TargetProjectID: &pid,
				TargetIssueIID:  &iid,
			}
			_, _, err := pc.GitLabClient.IssueLinks.CreateIssueLink(i.ObjectAttributes.ProjectID, i.ObjectAttributes.IID, linkOpts)
			if err != nil {
				logrus.WithError(err).Warn("Failed to create issue link")
				continue
			}
		}
	}

	return nil
}
