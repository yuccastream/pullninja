package plugins

import (
	"strings"
	"sync"

	"github.com/xanzy/go-gitlab"

	"gitlab.com/yuccastream/pullninja/pkg/utils"
)

var (
	mergeRequestHandlers = map[string]MergeRequestHandler{}
	mergeCommentHandlers = map[string]MergeCommentHandler{}
	issueHandlers        = map[string]IssueHandler{}
	issueCommentHandlers = map[string]IssueCommentHandler{}
	pipelineHandlers     = map[string]PipelineHandler{}
)

type (
	MergeRequestHandler func(*PluginClient, *gitlab.MergeEvent) error
	MergeCommentHandler func(*PluginClient, *gitlab.MergeCommentEvent) error
	IssueHandler        func(*PluginClient, *gitlab.IssueEvent) error
	IssueCommentHandler func(*PluginClient, *gitlab.IssueCommentEvent) error
	PipelineHandler     func(*PluginClient, *gitlab.PipelineEvent) error
)

type PluginClient struct {
	GitLabClient *gitlab.Client
	GitLabUser   *gitlab.User
	mut          sync.RWMutex
	Config       *Config
}

func RegisterMergeCommentHandler(name string, fn MergeCommentHandler) {
	mergeCommentHandlers[name] = fn
}

func RegisterMergeRequestHandler(name string, fn MergeRequestHandler) {
	mergeRequestHandlers[name] = fn
}

func RegisterIssueHandler(name string, fn IssueHandler) {
	issueHandlers[name] = fn
}

func RegisterIssueCommentHandler(name string, fn IssueCommentHandler) {
	issueCommentHandlers[name] = fn
}

func RegisterPipelineHandler(name string, fn PipelineHandler) {
	pipelineHandlers[name] = fn
}

func (p *PluginClient) MergeCommentHandlers(pathWithNamespace string) map[string]MergeCommentHandler {
	handlers := map[string]MergeCommentHandler{}
	for _, plugin := range p.getPlugins(pathWithNamespace) {
		if h, ok := mergeCommentHandlers[plugin]; ok {
			handlers[plugin] = h
		}
	}
	return handlers
}

func (p *PluginClient) MergeRequestHandlers(pathWithNamespace string) map[string]MergeRequestHandler {
	handlers := map[string]MergeRequestHandler{}
	for _, plugin := range p.getPlugins(pathWithNamespace) {
		if h, ok := mergeRequestHandlers[plugin]; ok {
			handlers[plugin] = h
		}
	}
	return handlers
}

func (p *PluginClient) IssueHandlers(pathWithNamespace string) map[string]IssueHandler {
	handlers := map[string]IssueHandler{}
	for _, plugin := range p.getPlugins(pathWithNamespace) {
		if h, ok := issueHandlers[plugin]; ok {
			handlers[plugin] = h
		}
	}
	return handlers
}

func (p *PluginClient) IssueCommentHandlers(pathWithNamespace string) map[string]IssueCommentHandler {
	handlers := map[string]IssueCommentHandler{}
	for _, plugin := range p.getPlugins(pathWithNamespace) {
		if h, ok := issueCommentHandlers[plugin]; ok {
			handlers[plugin] = h
		}
	}
	return handlers
}

func (p *PluginClient) PipelineHandlers(pathWithNamespace string) map[string]PipelineHandler {
	handlers := map[string]PipelineHandler{}
	for _, plugin := range p.getPlugins(pathWithNamespace) {
		if h, ok := pipelineHandlers[plugin]; ok {
			handlers[plugin] = h
		}
	}
	return handlers
}

func (p *PluginClient) getPlugins(pathWithNamespace string) []string {
	p.mut.Lock()
	defer p.mut.Unlock()

	var plugins []string

	parts := GetNamespaceParts(pathWithNamespace)
	for _, part := range parts {
		plugins = append(plugins, p.Config.Plugins[part]...)
	}
	plugins = utils.RemoveDuplicatesUnordered(plugins)

	return plugins
}

func GetNamespaceParts(ns string) []string {
	ns = strings.Replace(ns, " / ", "/", -1)
	parts := strings.Split(ns, "/")
	parts = append(parts, ns)
	subparts := []string{}
	for index := 0; index < len(parts); index++ {
		part := strings.Join(parts[0:index], "/")
		if len(part) == 0 {
			continue
		}
		subparts = append(subparts, part)
		part = strings.Join(parts[index:len(parts)-1], "/")
		if len(part) == 0 {
			continue
		}
		subparts = append(subparts, part)
	}
	parts = append(parts, subparts...)
	parts = utils.RemoveDuplicatesUnordered(parts)
	return parts
}
